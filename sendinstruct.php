<?php
include "classes/sendrequest.php";
include "config.php";
session_start();
$where=$_GET['where'];
if($_SERVER['REQUEST_METHOD']=="POST")
{
      //send api request
  

       $name=$_POST['name'];
       $account=$_POST['account'];
       $amount=$_POST['amount'];
       $narration=$_POST['narration'];
       //print_r($_POST);die;
       $depositor_name=$_POST['depositor_name'];
       $depositor_contact=$_POST['depositor_contact'];

       $cheque_no=$_POST['cheque_no'];
       $FrontdataURL=$_POST['FrontdataURL'];
       $BackdataURL=$_POST['BackdataURL'];
       $cheque_accno=$_POST['cheque_accno'];

       if($amount=="" ){
        $arr = array('logincode' => '111',
                       'message' => 'Amount is required');
        echo json_encode($arr);
        exit();
       }

       if($narration=="" ){
        $arr = array('logincode' => '111',
                       'message' => 'Narration is required');
        echo json_encode($arr);
        exit();
       }

       if($depositor_name=="" ){
        $arr = array('logincode' => '111',
                       'message' => 'Depositor name is required');
        echo json_encode($arr);
        exit();
       }

       $method="PUT";

       
       if($where=='d'){
        $data=    'accountId='.$account.
                 '&amount='.$amount.
                 '&documentRef='.time().
                 '&narration='.$narration.
                 '&postBy='.$_SESSION['USERNAME'].
                 '&appBy='.$_SESSION['USERNAME'].
                 '&customerTel='.$depositor_contact;
         $link=$ip."account/".$account."/deposit";

          $request= new sendRequest($data,$method,$link);
       $response=$request->send();
       }
     

      
       //get response
     //echo $response;die;     

       if($where=='c'){
         $method="POST";
         $link= "http://10.1.1.24:5908/core/api/v1.0/account/cheque/deposit";
         $data=  'creditAccountNumber='.$account.
                 '&amount='.$amount.
                 '&docRef='.time().
                 '&narration='.$narration.
                 '&postBy='.$_SESSION['USERNAME'].
                 '&appBy='.$_SESSION['USERNAME'].
                 '&postTerminal='.$method.
                 '&customerTel='.$depositor_contact.
                 '&transBy='.$_SESSION['USERNAME'].
                 '&debitChequeNumber='.$cheque_no.
                   '&debitAccountNumber='.$cheque_accno.
                   '&frontImage='.$FrontdataURL.
                   '&backImage='.$BackdataURL;

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_PORT => "5908",
              CURLOPT_URL => "http://10.1.1.24:5908/core/api/v1.0/account/cheque/deposit",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $data,
              CURLOPT_HTTPHEADER => array(
                "Content-Type: application/x-www-form-urlencoded",
                "Postman-Token: 5291c330-036b-4e9e-abc0-0f7f3934e2eb",
                "cache-control: no-cache",
                "x-api-key: 201808081448",
                "x-api-secret: 130617044"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            
       }

         $decodedResponse=json_decode($response);
       if(isset($decodedResponse->responseCode))
       {
         if($decodedResponse->responseCode==0){
           //successful

          $arr = array('logincode' => '000',
                       'message' => $decodedResponse->message);


         }else{
           //successful

          $arr = array('logincode' => '111',
                       'message' => $decodedResponse->message);

         }

      echo json_encode($arr);
      exit();
     }
     else
     {
       //login fail
        $arr = array('logincode' => '111',
                     'message'=>'An Error has occured. Please try Again');
        echo json_encode($arr);
        exit();

     }
}

?>
