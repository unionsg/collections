<?php
session_start();
if(!isset($_SESSION['FULLNAME'])){
	header('location : index.php');

}
?>
<!DOCTYPE HTML>
<html>
<?php include 'headerscript.php';?>

<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1" >
		<!--left-fixed -navigation-->
		<?php include 'nav.php';?>
		<!--left-fixed -navigation-->

		<?php include 'header.php';?>
		<div id="page-wrapper">
			<div class="main-page">

		<!--START OF TAB-->
		<div class="alert alert-danger off" id='messageerror'></div>
		<div class="alert alert-success off" id='messagesuccess'></div>
		<div id='maintab'>
		<ul class="nav nav-tabs">
 	    	<li class="active"><a data-toggle="tab"  href="#deposit">Statement</a></li>
		  </ul>

		  <div class="tab-content tabcontent" >

			    <div id="deposit" class="tab-pane fade in active">
			     		<!---START OF FORM --->
							<div class="inline-form widget-shadow">
						<div class="form-title">
							<h4>Transaction Statement</h4>
						</div>
						<div class="form-body">
								<form class="form-horizontal">
									<div class="form-group" >
										<div class='row'>
												<div class="col-sm-5">
														<label for="inputEmail3" class="col-sm-2 control-label">From</label>
														<div class="form-group">
												            <div class='input-group date' id="picker_from">
												                <input type='text' id='date_from' value="<?php echo date('d-M-Y'); ?>"  data-date-format="DD-MMM-YYYY" class="form-control" />
												                <span class="input-group-addon">
												                    <span class="glyphicon glyphicon-calendar"></span>
												                </span>
												            </div>
												        </div>
													
												</div>

												<div class="col-sm-5">
													<label for="inputEmail3" class="col-sm-2 control-label">To</label>
													<div class="form-group">
												            <div class='input-group date' id="picker_to" >
												                <input type='text' id='date_to' value="<?php echo date('d-M-Y'); ?>"  data-date-format="DD-MMM-YYYY" data-date-end-date="0d"  class="form-control" />
												                <span class="input-group-addon">
												                    <span class="glyphicon glyphicon-calendar"></span>
												                </span>
												            </div>
												        </div>
												</div>

												<div class="col-sm-3">
													<br/>
													<input type="button"  onclick="search();" class="btn btn-default" value="Search"/>
 												  <img src="images/loader.gif" class='off loaderd' id='loaderd' height="10px;">
												</div>
										</div>
										<!--======================statement here==============================-->
										<div class='row'>
											<div class="col-sm-13">
												<div class="bs-example widget-shadow" data-example-id="hoverable-table" style='padding:10px;'>

													<table id="example" class="table display" style="width:100%">
									        <thead>
									            <tr>
									                <th>Posted_Date</th>
									                <th>Account No</th>
									                <th>Narration</th>
									                <th>Doc Ref</th>
									                <th>Amount</th>
																	<th>Currency</th>
																	<th></th>
									            </tr>
									        </thead>
									        <tbody id='tablebody'>

														</tbody>
													</table>
											</div>
											</div>
										</div>
										<!--======================end statement ==============================-->


										</div>



								 </form>
							</div>
					</div>
							<!---END OF FORM--->

			    </div>


		  </div>
					<!--END OF TAB-->
		</div>
				<!--END OF MAINTAB-->



		</div>




			</div>
		</div>
		
	<?php include 'footer.php';?>


<!----------------------------->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style='width:90%'>

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <center><strong> <h2  class="modal-title" style='color:#000'>Please Choose A Company</h2></strong></center>
        </div>
        <div class="modal-body">

			<!------------------------------------->

		<?php

		$decodedCollection=$_SESSION['COMPANIES'];
		//print_r($decodedCollection);
		foreach($decodedCollection as $key => $details) {
		?>

						<div class="col-sm-3">
							<div class="card" style="width:77%">
						    <img class="card-img-top" src="<?php if($details->logo!=''){echo 'data:image/png;base64,'.$details->logo;}else{ echo 'images/img_avatar1.png';}?>" alt="Card image" style="width:200px;height:200px;">
						    <div class="card-body">
						     <center> <h4 class="card-title"><strong><?php echo $details->name;?></strong></h4>
						      <a href="#" onclick="populate('<?php echo $details->name;?>','<?php echo $details->account;?>');"class="btn btn-primary" data-dismiss="modal">Select</a></center>
						    </div>
						  </div>

						</div>

			<?php }?>

			<!------------------------------------>


        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>
<!----------------------------->






</body>
<script>

		function stopLoading()
		{
			setTimeout(function(){
					$('.loaderd').hide();
					return;
			},2000);


		}
		function startLoading()
		{
						document.getElementById("tablebody").innerHTML="";
					$('.loaderd').show();
					return;
		}


			 //posting
			 function search(){
				 var from =$('#date_from').val();
				 var to =$('#date_to').val();
				 if(from.lenght<2 || to.length<2){
					 alert('invalid date selected');
					 return;
				 }
				 	console.log(from);
				 	console.log(to);
				 
				 var mydata = {
												 from:from,
												 to:to,
											};
				 startLoading();
				 $.post("trans_exec.php",mydata,
						function(data,status){
							 stopLoading();
							//return
							console.log(data);
							console.log(status);
							var obj;

						try
							{
								console.log('here');
								var obj = JSON.parse(data);
								// obj=data;
								console.log(obj.transactions);

								if(obj.logincode!="000")
								{
									alert(obj.message);
									return;
								}
									//	console.log(obj.transactions);
								for (var index in obj.transactions)
								{

									// console.log(obj.transactions[index].postingDate);
									// console.log(obj.transactions[index].acctLink);
									// console.log(obj.transactions[index].transactionDetails);
									// console.log(obj.transactions[index].documentRef);
									// console.log(obj.transactions[index].amt);
									// console.log(obj.transactions[index].currency);
									// console.log(obj.transactions[index].amtSign);
									if(obj.transactions[index].amtSign=="D"){
										var sign="<font color='red'><i class='fa fa-arrow-up'></i></font>";
									}else{
										var sign="<font color='green'><i class='fa fa-arrow-down'></i></font>";
									}

									document.getElementById("tablebody").innerHTML+=
										"<tr>"+
										"<td>"+obj.transactions[index].postingDate+"</td>"+
										"<td>"+obj.transactions[index].contraAccount+"</td>"+
										"<td>"+obj.transactions[index].transactionDetails+"</td>"+
										"<td>"+obj.transactions[index].documentRef+"</td>"+
										"<td>"+obj.transactions[index].amt+"</td>"+
										"<td>"+obj.transactions[index].currency+"</td>"+
										"<td>"+sign+"</td>"+
										"</tr>";

								}
									$('.odd td').html("Transaction statement From <b>"+from+"</b> To <b>"+to+"</b>");


							}
						catch (e)
						{
							alert('Error fething statement');
							return false;
							}
							validcode=obj.logincode;
							if(obj){
								if(validcode==="000")
								{
									//success


										return;
								}
								else
								{
										alert('Error fetching statment');
										return
								}
							}else{
								alert('Error fetching statement');
								return;
							}

					});


			 }




				$(document).ready(function() {

				    $('.table').DataTable();
						search();

				} );
</script>


<script type="text/javascript">
	 $(function () {
        $('#picker_from').datetimepicker({
        	format : "dd-M-yyyy"
        });
        $('#picker_to').datetimepicker({
            useCurrent: false,
            format : "dd-M-yyyy" //Important! See issue #1075           
        });
      
        $("#picker_from").on("dp.change", function (e) {
            $('#picker_to').data("DateTimePicker").minDate(e.date);
        });
        $("#picker_to").on("dp.change", function (e) {
            $('#picker_from').data("DateTimePicker").maxDate(e.date);
        });
    });

</script>


</html>
