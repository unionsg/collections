
<!DOCTYPE HTML>
<html>
<head>
<title>Collection Module</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<link href="css/bootstrap-datetimepicker.min.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS-->
<link href="css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons CSS-->


 <!-- js-->
<script src="js/jquery.js"></script>
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!-- Metis Menu -->
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
html, body {
    background: #dbdbdb;
}
#page-wrapper {
    background-color: #dbdbdb;
}

</style>

</head>
<body >
  <div style='width:100%;height:60px;background-color:#ec1d25'><center><h3 style='color:#fff;padding:15px'>SIERRA LEONE COMMERCIAL BANK COLLECTION MODULE</h3></center></div>
<div class="main-content">



		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page login-page ">

				<div class="widget-shadow">
					<div class="login-body">
						<form>
              <div class="alert alert-danger off" id='messageerror'></div>
              <div class="alert alert-success off" id='messagesuccess'></div>
							<input type="email" class="user" name="username" id='username' placeholder="Enter Your Username" required="">
							<input type="password" name="password" id='password' class="lock" placeholder="Password" required="">

							<input type="submit" name="Sign In" id='login' value="Sign In" style='background-color:#ec1d25'>
              <center><img src="images/loader.gif" class='off' id='loader' height="15px;"></center>
						</form>
					</div>
				</div>

			</div>
		</div>






	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->

	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   <script>


             $('#login').click(function () {

                var username=$('#username').val();
                var password=$('#password').val();
                if(username.length<1 || password.length<1)
                {
                showMessage('Invalid username or password','error');
                return;
                }

                 var data=   'username='+username+
                            '&password='+password;
                var mydata = {
                                 data:data,
                                 method: 'POST'
                              };
                  $('#loader').show();
                  $("#login").attr('disabled', 'disabled');

                  //posting
              	   $.post("loginexec.php",mydata,
              			  function(data,status){
                        //return
              					console.log(data);
                        var obj;
                      try
                        {
                          obj = JSON.parse(data);
                        }
                      catch (e)
                      {
                          showMessage('An Error has occured. please try again','error');
                          stopLoading();
                        return false;
                        }

                        if(obj){
                          var validcode=obj.logincode;
                          var message=obj.message;
                          if(validcode==="000")
                          {
                              showMessage(message,'success');
                              window.location.assign('home.php');
                              return;
                          }
                          else
                          {
                              showMessage(message,'error');
                              stopLoading();
                              return
                          }
                        }else{
                          showMessage('An error has occured. Please try again','error');
                          stopLoading();
                          return;
                        }
                        $('#loader').hide();
                        $("#login").attr('disabled', false);

              			});
        	 });

        function showMessage(content,type)
        {

                $('#messagesuccess').hide();
                $('#messageerror').hide();
                if(type=='error')
                {
                    $('#messageerror').show();
                    $('#messageerror').html(content);
                }
                else
                {
                    $('#messagesuccess').show();
                    $('#messagesuccess').html(content);
                }
                return;
        }
       function stopLoading()
       {
         $('#loader').hide();
         $("#login").attr('disabled', false);
         $('#password').val("");
         return;
       }
   </script>
</body>
</html>



















}
