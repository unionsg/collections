<?php
include "classes/sendrequest.php";
include "config.php";
session_start();
if($_SERVER['REQUEST_METHOD']=="POST")
{
      //send api request
      // tout_amount:tout_amount,
      // tout_branch: tout_branch,
      // where:'tout'
       $where=$_POST['where'];
       $method="POST";
       if($where=="tin")
       {
         $tin_amount=$_POST['tin_amount'];
         $tin_branch=$_POST['tin_branch'];

         $data="amount=".$tin_amount.
                "&treasuryType=TREASURY_IN".
                "&branchCode=".$tin_branch.
                "&postedBy=".$_SESSION['USERNAME'].
                "&postingTerminal=";
       }


       if($where=="tout")
       {
         $tout_amount=$_POST['tout_amount'];
         $tout_branch=$_POST['tout_branch'];
         $data="amount=".$tout_amount.
                "&treasuryType=TREASURY_OUT".
                "&branchCode=".$tout_branch.
                "&postedBy=".$_SESSION['USERNAME'].
                "&postingTerminal=";
       }

       //sending request
       $request= new sendRequest($data,$method,$ip.'user/treasury');
       $response=$request->send();

       //get response
       $decodedResponse=json_decode($response);
       if(isset($decodedResponse->responseCode))
       {
        //successful
          if($decodedResponse->responseCode!="000")
          {
            $arr = array('logincode' => '111',
                         'message' => $decodedResponse->message);
            echo json_encode($arr);
            exit();
          }


           $arr = array('logincode' => '000',
                        'message' => $decodedResponse->message." Token No ".$decodedResponse->token
                      );
            echo json_encode($arr);
                         exit();
     }
     else
     {
       //login fail
        $arr = array('logincode' => '111',
                     'message' => 'An Unexpected error has occured');
        echo json_encode($arr);
        exit();

     }
}

?>
