<?php
include "classes/sendrequest.php";
include "config.php";
session_start();
if($_SERVER['REQUEST_METHOD']=="POST")
{
      //send api request
       $accountId=$_POST['accountId'];
       $method="GET";
       $user= $_SESSION['USERNAME'];
       $request= new sendRequest(null,$method,$ip.'user/'.$user.'/balance');
       $response=$request->send();
       //get response
       $decodedResponse=json_decode($response);
       if(isset($decodedResponse->avBalance))
       {
        //successful
       $_SESSION['accountName'] =   $decodedResponse->accountName;
       $_SESSION['avBalance'] = $decodedResponse->avBalance;
       $_SESSION['avBalanceSsh'] = $decodedResponse->avBalanceSsh;
       $_SESSION['bkBalance'] = $decodedResponse->bkBalance;
       $_SESSION['blkAmt'] = $decodedResponse->blkAmt;
       $_SESSION['brCode'] = $decodedResponse->brCode;
       $_SESSION['clearedBalance'] = $decodedResponse->clearedBalance;
       $_SESSION['lienAmt'] = $decodedResponse->lienAmt;
       $_SESSION['noOfHolder'] = $decodedResponse->noOfHolder;
       $_SESSION['odAmount'] = $decodedResponse->odAmount;
       $_SESSION['product'] = $decodedResponse->product;
       $_SESSION['riskCode'] = $decodedResponse->riskCode;
       $_SESSION['unClearedBalance'] = $decodedResponse->unClearedBalance;

       $arr = array('logincode' => '000',
                    'accountName' => $decodedResponse->accountName,
                    'avBalance' => $decodedResponse->avBalance,
                    'avBalanceSsh' => $decodedResponse->avBalanceSsh,
                    'bkBalance' => $decodedResponse->bkBalance,
                    'brCode' => $decodedResponse->brCode,
                    'clearedBalance' => $decodedResponse->clearedBalance,
                    'lienAmt' => $decodedResponse->lienAmt,
                    'noOfHolder' => $decodedResponse->noOfHolder,
                    'odAmount' => $decodedResponse->odAmount,
                    'product' => $decodedResponse->product,
                    'riskCode' => $decodedResponse->riskCode,
                    'unClearedBalance' => $decodedResponse->unClearedBalance);


      echo json_encode($arr);
       exit();
     }
     else
     {
       //login fail
        $arr = array('logincode' => '111');
        echo json_encode($arr);
        exit();

     }
}

?>
