$(document).ready(function(){
		$('.loader').show();
		$(".nav-tabs a").click(function(){
				$(this).tab('show');
		});
		$('#d_spinner').hide();
		$('#w_spinner').hide();
		$('#maintab').hide();
		processBalance(1,null);
		//startModal()
});



		function stopLoading()
		{
			$('.loader').hide();

			return;
		}

		function processBalance(a,type){

			if(a==1 && type==null){//normal Rm check balance
				var data="<?php echo $_SESSION['ACC'];?>";
			}else{
					if( type=='deposit'){ //Client check balance
						$('#d_spinner').show();
						var data=$('#d_accountno').val();

					}else{
							$('#w_spinner').show();
						var data=$('#w_accountno').val();
					}

					if(data.length<18){
						setTimeout(function(){
							$('#d_spinner').hide();
							$('#w_spinner').hide();
						},1000);

						return;
					}

			}

			console.log(data);
		 var mydata = {
											accountId:data,
											method: 'POST'
									 };
				if(a==1){
					 $('.loader').show();
				}


			 //posting
				$.post("loadbalance.php",mydata,
					 function(data,status){
						 //return
						 console.log(data);
						 var obj;
					 try
						 {
							 obj = JSON.parse(data);
						 }
					 catch (e)
					 {
							 showMessage('Error loading balance','error',a);
						 return false;
						 }
						 validcode=obj.logincode;
						 if(obj){
							 if(validcode==="000")
							 {
								 //success
								 	if(a==1){//normal teller balance check
										var avbal=obj.avBalance;
										//var bookbal=obj.bkBalance;
										//var lienbal=obj.lienAmt;
										var brCode=obj.brCode;

										$('#avbal').html(" "+avbal);
										//$('#bookbal').html(" "+bookbal);
										//$('#lienbal').html(" "+lienbal);
										$('#branch').html(" "+brCode);

										$('.loader').hide();
										$('#d_spinner').hide();
										$('#w_spinner').hide();
									}else{
												$('#d_spinner').hide();
												$('#w_spinner').hide();
											if( type=='deposit'){ //deposit
												$('#d_accname').val(obj.accountName);
												$('#d_bban').val(obj.accountName);
											}else{//withdraw
												$('#w_accname').val(obj.accountName);
												$('#w_bban').val(obj.accountName);
											}

									}

									 return;
							 }
							 else
							 {
									 showMessage('Error loading balance','error',a);
									 return
							 }
						 }else{
							 showMessage('Error loading balance','error',a);
							 return;
						 }

				 });
		}
		function showMessage($message,$type,a){

			$("#messageerror").show();
			$("#messageerror").html($message);

			if(a==1){
				$('.loader').hide();
				hideMessage();
				return;
			}
			$('#d_spinner').hide();
			$('#w_spinner').hide();
			hideMessage();
			return;
			//$('#avbal').val($message);
			//$('#bookbal').val($message);
			//$('#lienbal').val($message);
		}

			function startModal()
			{
					  $("#myModal").modal({
							backdrop: 'static',
			    		keyboard: false
						});
						$('#maintab').hide('slow');
						return;
			}
			function populate(name,account)
			{
								$('#w_accname').val(name);
								$('#w_bban').val(account);
								$('#d_accname').val(name);
								$('#d_bban').val(account);
								$('#cheque_bban').val(account);
								$('#cheque_accname').val(name);
								$('#maintab').show('slow');
								//$('html, body').animate({scrollTop:$(document).height()}, 'slow');
								//$('#withdraw').hide();
								//$('#withdrawl').hide();
								return;
			}

			function sendInstruction(type)
					{
						if(type=='deposit')
						{
							var name=	$('#d_accname').val();
							var account=	$('#d_bban').val();
							var amount=	$('#d_amount').val();
							var narration=	$('#d_narration').val();
							var depositor_name=	$('#d_depositor_name').val();
							var depositor_contact=	$('#d_depositor_contact').val();
							loaderspin();
						}

						var FrontdataURL="";
						var BackdataURL="";
						var cheque_no="";
						var cheque_accno="";
						
						if(type=='cheque')
						{
							var front_canvas = document.getElementById('front_canvas');
							var FrontdataURL = front_canvas.toDataURL();
							var back_canvas = document.getElementById('back_canvas');
							var BackdataURL = back_canvas.toDataURL();

							var name=	$('#cheque_accname').val();
							var account=	$('#cheque_bban').val();
							var amount=	$('#cheque_amount').val();
							var narration=	$('#cheque_narration').val();
							var cheque_no=	$('#cheque_no').val();
							var depositor_name=	$('#cheque_depositor_name').val();
							var depositor_contact=	$('#cheque_depositor_contact').val();
							var cheque_accno=$('#cheque_accno').val();
							loaderspin();
						}
						var mydata = {
														 name:name,
														 account: account,
														 amount:amount,
														 narration:narration,
														 depositor_name:depositor_name,
														 depositor_contact:depositor_contact,
														 cheque_no:cheque_no,
														 FrontdataURL:FrontdataURL,
														 BackdataURL:BackdataURL,
														 cheque_accno:cheque_accno
													};
							if(type=='deposit')
							{
								SendApiRequest('sendinstruct.php?where=d',mydata);
								return;
							}

							if(type=='cheque')
							{

								SendApiRequest('sendinstruct.php?where=c',mydata);
								return;
							}
			}

			function SendApiRequest(url,mydata){

				$.post(url,mydata,
					 function(data,status){
						 //return
						 console.log(data);
						 var obj;
					 try
						 {
							 obj = JSON.parse(data);
							 if(obj)
							 {
								 if(obj.logincode=="000")
								 {
									 stopLoaderSpin();
									 throwSuccess(obj);
									 return;
								 }
								 else if(obj.logincode=="111")
								 {
								 	 errorthrow(obj.message);
									 stopLoaderSpin();
									 return;
								 }
								 else
								 {
									 errorthrow('An Error has occured. Please try Again');
									 stopLoaderSpin();
									 return;
								 }

							 }



						 }
					 catch (e)
					 {
						 	stopLoaderSpin();
							console.log(e);
							alert('An Error has occured...');
						 return 1;
					 }
				 });

			}
				function errorthrow(message)
				{
					$("#messageerror").html(message);
					$("#messageerror").show();
					hideMessage()
					return;
				}
				function loaderspin()
				{
					$('#loaderd').show();
					$('#loaderw').show();
					disableButton();
				}

				function stopLoaderSpin()
				{
					$('#loaderd').hide();
					$('#loaderw').hide();
					enableButton();
				}

				function 	throwSuccess(resultObj)
				{
					$("#messagesuccess").show();
					$("#messagesuccess").html(resultObj.message);
					var brlenghtcompare=$("#d_amount").val();

					if(brlenghtcompare.length!=0)
					{
						//cash withdrawal
								var where=resultObj.message+" Cash Deposit. Narration- "+$('#d_narration').val()+" ";
					}

					else
					{
							//cheque withdrawl
								var brcode=$("#tout_branch").val();
								var where=resultObj.message+" Cheque Deposit. Narration- "+$('#cheque_narration').val()+" Cheque no- "+$('#cheque_no').val()+" Cheque Account -"+$('#cheque_accno').val();
					}

					var link="print.php?token="+resultObj.message
									+"&amount="+	$("#d_amount").val()+	$("#cheque_amount").val()
									+"&description="+where;

					$('#d_amount').val("");
					$('#d_narration').val("");
					$('#depositor_contact').val("");
					$('#d_depositor_name').val("");
					$('html, body').animate({scrollTop:0}, 'slow');

					setTimeout(function(){

						mywindow = window.open(link, "mywindow", "location=1,status=1,scrollbars=1,  width=1000,height=700");
							 mywindow.moveTo(0, 0);
					},1000);




					stopLoaderSpin();
					enableButton();
					hideMessage();
				}

				function hideMessage()
				{
						setTimeout(function(){
								$("#messagesuccess").hide();
								$("#messageerror").hide();
						},10000);
				}
				function disableButton(){
					document.getElementById("d_deposit").disabled = true;
					document.getElementById("w_withdraw").disabled = true;
					return;
				}

				function enableButton(){
					document.getElementById("d_deposit").disabled = false;
					document.getElementById("w_withdraw").disabled = false;
					return;
				}
				function viewMandate()
				{
					var cheque_accno=$('#cheque_accno').val();
					console.log('started');
					$.post( "viewMandate.php?account="+cheque_accno, function( data ) {


								try
									{
										obj = JSON.parse(data);
										if(obj)
										{
											if(obj.logincode=="000")
											{
												stopLoaderSpin();
												console.log('success');
												processMandate(obj);
												return;
											}
											else
											{
													errorthrow('An Error has occured. Please try Again');
													stopLoaderSpin();
													return;
											}

										}



									}
								catch (e)
								{
									 stopLoaderSpin();
									 alert('An Error has occured');
									return 1;
								}
					});
				}

	function processMandate(obj){
		console.log('processing mandate');
		$("#mandatebody").html("");
		for (var index in obj.mandates)
		{

			// console.log(obj.transactions[index].postingDate);
			// console.log(obj.transactions[index].acctLink);
			// console.log(obj.transactions[index].transactionDetails);
			// console.log(obj.transactions[index].documentRef);
			// console.log(obj.transactions[index].amt);
			// console.log(obj.transactions[index].currency);
			console.log(obj.mandates[index].accountname);

			document.getElementById("mandatebody").innerHTML+=
				"<div class='row'>"+
				"<div class='col-sm-12'><center><b>"+obj.mandates[index].accountname+"</b></center></div>"+
				"</div>"+
				"<div class='row' style='padding:10px;'>"+
				"<div class='col-sm-6'><center>"+"<img class='card-img-top' src='data:image/png;base64,"+obj.mandates[index].signature+"' style='width:300px;'></center>"+"</div>"+
				"<div class='col-sm-6'><center>"+"<img class='card-img-top' src='data:image/png;base64,"+obj.mandates[index].photograph+"' style='width:300px;'></center>"+"</div>"+
				"</div>";

		}


		$("#mandatemodal").modal({
			backdrop: 'static',
			keyboard: false
		});
		return;
	}
