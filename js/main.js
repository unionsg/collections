/*
 *  Copyright (c) 2015 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */

 

'use strict';
		// Put variables in global scope to make them available to the browser console.
const fvideo = document.getElementById('front');
const bvideo = document.getElementById('back');

function handleFrontSuccess(stream) {
  window.stream = stream; // make stream available to browser console
  fvideo.srcObject = stream;
}

function handleBackSuccess(stream) {
  window.stream = stream; // make stream available to browser console
  bvideo.srcObject = stream;
}

function handleError(error) {
  console.log('navigator.getUserMedia error: ', error);
}

function start_front()
{
	
	const canvas = window.canvas = document.getElementById('front_canvas');
	canvas.width = 280;
	canvas.height = 160;
	
	const constraints = {
	  audio: false,
	  video: true
	};
	
	
	navigator.mediaDevices.getUserMedia(constraints).then(handleFrontSuccess).catch(handleError);
	$('html, body').animate({scrollTop:$(document).height()}, 'slow');

}

function start_back()
{
	var canvas=document.getElementById('back_canvas');
	canvas = window.canvas = document.getElementById('back_canvas');
	canvas.width = 280;
	canvas.height = 160;
	
	const constraints = {
	  audio: false,
	  video: true
	};
	
	
	navigator.mediaDevices.getUserMedia(constraints).then(handleBackSuccess).catch(handleError);
	$('html, body').animate({scrollTop:$(document).height()}, 'slow');
}


function snap_front(){
	  var canvas=document.getElementById('front_canvas');
	  document.getElementById('front_canvas').width =  document.getElementById('front').videoWidth;
	  document.getElementById('front_canvas').height =  document.getElementById('front').videoHeight;
	  canvas.getContext('2d').drawImage(document.getElementById('front'), 0, 0, document.getElementById('front_canvas').width,  document.getElementById('front_canvas').height);
	  $('html, body').animate({scrollTop:$(document).height()}, 'slow');
}

function snap_back(){
	  document.getElementById('back_canvas').width =  document.getElementById('back').videoWidth;
	  document.getElementById('back_canvas').height =  document.getElementById('back').videoHeight;
	  canvas.getContext('2d').drawImage(document.getElementById('back'), 0, 0, document.getElementById('back_canvas').width,  document.getElementById('back_canvas').height);
	  $('html, body').animate({scrollTop:$(document).height()}, 'slow');
}


