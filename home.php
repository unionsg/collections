
<!DOCTYPE HTML>
<html>
<?php include 'headerscript.php';?>
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1" >
		<!--left-fixed -navigation-->
		<?php include 'nav.php';?>
		<!--left-fixed -navigation-->

		<!-- header-starts -->
		<?php include 'header.php';?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
			<div class="row">


        	<div class="col-md-12 widget widget1" style='margin-top:-20px;'>
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
                    <div class="stats">
											<div class='row'>
												<div class="col-sm-10"><center> <strong> <h2 style='color:#ccc'><?php echo  $_SESSION['FULLNAME'];?></h2></strong><a href="#" onclick="startModal();"><i class="fa fa-home" style="font-size:50px"></i><br/>Click to choose a company</a>

</center></div>
											</div>


										<div class='row'>
											<div class="col-sm-6">Cash On Hand: <strong class='bal' id='avbal'>  <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>
											<!--<div class="col-sm-3">Book Balance: <strong class='bal' id='bookbal'> <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>-->
											<!--<div class="col-sm-3">Lien Amount: <strong class='bal' id='lienbal'> <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>-->
											<div class="col-sm-6">Branch: <strong class='bal' id='branch'> <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>

										</div>

                    </div>
                </div>
        	</div>


        	<div class="clearfix"> </div>
		</div>
		<!--START OF TAB-->
		<div class="alert alert-danger off" id='messageerror'></div>
		<div class="alert alert-success off" id='messagesuccess'></div>
		<div id='maintab'>
		<ul class="nav nav-tabs">
 	    	<li class="active"><a data-toggle="tab"  href="#deposit">Deposit</a></li>
		    <li><a data-toggle="tab" class="off" id='withdrawl' href="#withdraw">Cheque Deposit</a></li>
		  </ul>

		  <div class="tab-content tabcontent" >

			    <div id="deposit" class="tab-pane fade in active">
			     		<!---START OF FORM --->
							<div class="inline-form widget-shadow">
						<div class="form-title">
							<h4>Deposit Cash</h4>
						</div>
						<div class="form-body">
								<form class="form-horizontal">
									<div class="form-group off" >
										<label for="inputEmail3" class="col-sm-2 control-label">Search BBAN</label>
										<div class="col-sm-9">
											<div class="input-icon right spinner">
													<i class="fa fa-fw fa-spin fa-spinner off" id='d_spinner'></i>
													<input id="d_accountno" oninput="processBalance(1,'deposit');" class="form-control1" type="text" placeholder="Search BBAN Here...">
												</div>
										</div>
									</div>
									<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">Account Name</label>
											<div class="col-sm-9"> <input type="text" class="form-control" id="d_accname" readonly >
											</div>
									 </div>
									 <div class="form-group">
 											<label for="inputPassword3" class="col-sm-2 control-label">BBAN</label>
 											<div class="col-sm-9"> <input type="text" class="form-control" id="d_bban" readonly>
 											</div>
 									 </div>

									 <div class="form-group">
 											<label for="inputPassword3" class="col-sm-2 control-label">Amount</label>
 											<div class="col-sm-3"> <input type="number" class="form-control" id="d_amount" placeholder="Enter an amount">
 											</div>
 									 </div>
									 <div class="form-group">
											 <label for="inputPassword3" class="col-sm-2 control-label">Narration</label>
											 <div class="col-sm-9"> <input type="text" class="form-control" id="d_narration" placeholder="Enter a narration">
											 </div>
										</div>

										<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Depositor Name</label>
												<div class="col-sm-9"> <input type="text" class="form-control" id="d_depositor_name" placeholder="Enter person who deposited">
												</div>
										 </div>

										 <div class="form-group">
												 <label for="inputPassword3" class="col-sm-2 control-label">Depositor Contact</label>
												 <div class="col-sm-9"> <input type="text" class="form-control" id="d_depositor_contact" placeholder="Enter Depositor Contact">
												 </div>
											</div>

									 <div class="col-sm-offset-2">
										 <input type="button" id='d_deposit' onclick="sendInstruction('deposit')" class="btn btn-default" value="Make Deposit"/>
										  <img src="images/loader.gif" class='off loaderd' id='loaderd' height="10px;">
									 </div>
								 </form>
							</div>
					</div>
							<!---END OF FORM--->

			    </div>
			    <div id="withdraw" class="tab-pane fade">
						<!---START OF FORM --->
						 <div class="inline-form widget-shadow">
					 <div class="form-title">
						 <h4>Deposit Cheque</h4>
					 </div>
					 <div class="form-body">
							 <form class="form-horizontal">
								 <div class="form-group off">
									 <label for="inputEmail3" class="col-sm-2 control-label">Search BBAN</label>
									 <div class="col-sm-9 ">
										 <div class="input-icon right spinner">
												 <i class="fa fa-fw fa-spin fa-spinner off" id='w_spinner'></i>
												 <input id="w_accountno"   oninput="processBalance(1,'withdraw');"  class="form-control1" type="text" placeholder="Search BBAN Here...">
											 </div>
									 </div>
								 </div>
								 <div class="form-group">
										 <label for="inputPassword3" class="col-sm-2 control-label">Account Name</label>
										 <div class="col-sm-9"> <input type="text" class="form-control" id="cheque_accname" readonly >
										 </div>
									</div>
									<div class="form-group">
										 <label for="inputPassword3" class="col-sm-2 control-label">BBAN</label>
										 <div class="col-sm-9"> <input type="text" class="form-control" id="cheque_bban" readonly>
										 </div>
									</div>

									<div class="form-group">
										 <label for="inputPassword3" class="col-sm-2 control-label">Amount</label>
										 <div class="col-sm-3"> <input type="number" class="form-control" id="cheque_amount" placeholder="Enter an amount">
										 </div>
									</div>
									<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">Narration</label>
											<div class="col-sm-9"> <input type="text" class="form-control" id="cheque_narration" placeholder="Enter a narration">
											</div>
									 </div>
									 <div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">Cheque No</label>
											<div class="col-sm-9"> <input type="text" class="form-control" id="cheque_no" placeholder="Enter cheque no ">
											</div>
									 </div>

									 <div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">Cheque Acc No</label>
											<div class="col-sm-6"> <input type="text" class="form-control" id="cheque_accno" placeholder="Enter cheque Account no ">
											</div>
											<div class="col-sm-3">  <input type="button" class='btn-primary form-control' onclick="viewMandate();" value="View Mandate"/>
											</div>
									 </div>

									 <div class="form-group">
											 <label for="inputPassword3" class="col-sm-2 control-label">Depositor Name</label>
											 <div class="col-sm-9"> <input type="text" class="form-control" id="cheque_depositor_name" placeholder="Enter person who deposited">
											 </div>
										</div>

										<div class="form-group">
												<label for="inputPassword3" class="col-sm-2 control-label">Depositor Contact</label>
												<div class="col-sm-9"> <input type="text" class="form-control" id="cheque_depositor_contact" placeholder="Enter Depositor Contact">
												</div>
										 </div>

										 <div class="form-group">
												 <label for="inputPassword3" class="col-sm-2 control-label">Scan Cheque</label>
												 <div class="col-sm-4">
													 <video playsinline autoplay id='front' class='video-border'></video>
													  <button class='form-control' onclick="start_front();">Start Front Cheque</button>
													  <button class='form-control' onclick="snap_front();">Snapshot Front Cheque</button>
														<br/>
														<canvas id='front_canvas' style='width:100%'></canvas>
												 </div>


												 <div class="col-sm-4">
													  <video playsinline autoplay id='back' class='video-border' ></video>
														 <button class='form-control' onclick="start_back();">Start Back Cheque</button>
														 <button class='form-control' onclick="snap_back();">Snapshot Back Cheque</button>
														 	<br/>
														 <canvas  id='back_canvas' style='width:100%'></canvas>
												</div>


											</div>



									<div class="col-sm-offset-2">
										<input type="button" id='w_withdraw' onclick="sendInstruction('cheque')" class="btn btn-default" value="Make Deposit"/>
  									<img src="images/loader.gif" class='off loaderw' id='loaderw' height="10px;">
									</div>
								</form>
						 </div>
				 </div>
						 <!---END OF FORM--->
			    </div>

		  </div>
					<!--END OF TAB-->
		</div>
				<!--END OF MAINTAB-->



		</div>




			</div>
		</div>
	<?php include 'footer.php';?>


<!----------------------------->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style='width:90%'>

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <center><strong> <h2  class="modal-title" style='color:#000'>Please Choose A Company</h2></strong></center>
        </div>
        <div class="modal-body">

			<!------------------------------------->

		<?php

		$decodedCollection=$_SESSION['COMPANIES'];
		//print_r($decodedCollection);
		foreach($decodedCollection as $key => $details) {
		?>

						<div class="col-sm-3">
							<div class="card" style="width:77%">
						    <img class="card-img-top" src="<?php if($details->logo!=''){echo 'data:image/png;base64,'.$details->logo;}else{ echo 'images/img_avatar1.png';}?>" alt="Card image" style="width:200px;height:200px;">
						    <div class="card-body">
						     <center> <h4 class="card-title"><strong><?php echo $details->name;?></strong></h4>
						      <a href="#" onclick="populate('<?php echo $details->name;?>','<?php echo $details->account;?>');"class="btn btn-primary" data-dismiss="modal">Select</a></center>
						    </div>
						  </div>

						</div>

			<?php }?>

			<!------------------------------------>


        </div>
        <div class="modal-footer">
        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
<!----------------------------->



<!--================= VIEW MANDATE==================================-->
<!-- Modal -->
<div id="mandatemodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:70%'>

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-body" id="mandatebody">
      </div>
      <div class="modal-footer">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    </div>

  </div>
</div>

<!--==================END VIEW MANDATE==============================-->




</body>
<script src="js/extras.js?ver=<?php echo time();?>"></script>
<script src="js/adapter-latest.js"></script>
<script src="js/common.js"></script>
<script src="js/main.js?ver=<?php echo time();?>" async></script>



</html>
