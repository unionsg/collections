
<!DOCTYPE HTML>
<html>
<?php include 'headerscript.php';?>
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1" >
		<!--left-fixed -navigation-->
		<?php include 'nav.php';?>
		<!--left-fixed -navigation-->

		<!-- header-starts -->
		<?php include 'header.php';?>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
			<div class="row">


        	<div class="col-md-12 widget widget1" style='margin-top:-20px;'>
        		<div class="r3_counter_box">
                    <i class="pull-left fa fa-money user2 icon-rounded"></i>
                    <div class="stats">

										<div class='row'>
											<div class="col-sm-6">Cash On Hand: <strong class='bal' id='avbal'>  <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>
											<!--<div class="col-sm-3">Book Balance: <strong class='bal' id='bookbal'> <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>-->
											<!--<div class="col-sm-3">Lien Amount: <strong class='bal' id='lienbal'> <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>-->
											<div class="col-sm-6">Branch: <strong class='bal' id='branch'> <img src="images/loader.gif" class='off loader' id='loader' height="10px;"></strong></div>

										</div>

                    </div>
                </div>
        	</div>


        	<div class="clearfix"> </div>
		</div>
		<!--START OF TAB-->
		<div class="alert alert-danger off" id='messageerror'></div>
		<div class="alert alert-success off" id='messagesuccess'></div>
		<div id='treasurytab'>
		<ul class="nav nav-tabs">
 	    	<li class="active"><a data-toggle="tab"  href="#tin">Treasury In</a></li>
		    <li><a data-toggle="tab"  href="#tout">Treasury Out</a></li>
		  </ul>

		  <div class="tab-content tabcontent" >

			    <div id="tin" class="tab-pane fade in active">
			     		<!---START OF FORM --->
							<div class="inline-form widget-shadow">
						<div class="form-title">
							<h4>Treasury In</h4>
						</div>
						<div class="form-body">
								<form class="form-horizontal">

									<div class="form-group">
											<label for="inputPassword3" class="col-sm-2 control-label">Amount</label>
											<div class="col-sm-9"> <input type="number" min='1' class="form-control" id="tin_amount"  >
											</div>
									 </div>
									 <div class="form-group">
 											<label for="inputPassword3" class="col-sm-2 control-label">Select Branch</label>
 											<div class="col-sm-9">
												<select class="form-control" id='tin_branch'>
													<option value=''>---select Branch---</option>
													<?php
													$decodedBranch=$_SESSION['BRANCHES'];
													foreach($decodedBranch as $key => $details) {
													?>
												<option value='<?php echo $details->code;?>'><?php echo $details->description;?></option>

														<?php }?>
														<select>
 											</div>
 									 </div>


									 <div class="col-sm-offset-2">
										 <input type="button" id='btn_tin' onclick="sendTreasuryInstruction('tin')" class="btn btn-default" value="Make Treasury In Request"/>
										  <img src="images/loader.gif" class='off loaderd' id='loaderd' height="10px;">
									 </div>
								 </form>
							</div>
					</div>
							<!---END OF FORM--->

			    </div>
			    <div id="tout" class="tab-pane">
						<!---START OF FORM --->
						 <div class="inline-form widget-shadow">
					 <div class="form-title">
						 <h4>Treasury Out</h4>
					 </div>


					 <div class="form-body">
				 			<form class="form-horizontal">

				 				<div class="form-group">
				 						<label for="inputPassword3" class="col-sm-2 control-label">Amount</label>
				 						<div class="col-sm-9"> <input type="number" min='1' class="form-control" id="tout_amount"  >
				 						</div>
				 				 </div>
				 				 <div class="form-group">
				 						<label for="inputPassword3" class="col-sm-2 control-label">Select Branch</label>
				 						<div class="col-sm-9">
				 							<select class="form-control" id='tout_branch'>
												<option value=''>---select Branch---</option>
												<?php
												$decodedBranch=$_SESSION['BRANCHES'];
												foreach($decodedBranch as $key => $details) {
												?>
											<option value='<?php echo $details->code;?>'><?php echo $details->description;?></option>

													<?php }?>
				 									<select>
				 						</div>
				 				 </div>


				 				 <div class="col-sm-offset-2">
				 					 <input type="button" id='btn_tout' onclick="sendTreasuryInstruction('tout')" class="btn btn-default" value="Make Treasury Out Request"/>
				 						<img src="images/loader.gif" class='off loaderd' id='loaderd' height="10px;">
				 				 </div>
				 			 </form>
				 		</div>
				 </div>
						 <!---END OF FORM--->
			    </div>

		  </div>
					<!--END OF TAB-->
		</div>
				<!--END OF MAINTAB-->



		</div>




			</div>
		</div>
	<?php include 'footer.php';?>


<!----------------------------->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog" style='width:90%'>

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">

          <center><strong> <h2  class="modal-title" style='color:#000'>Please Choose A Company</h2></strong></center>
        </div>
        <div class="modal-body">

			<!------------------------------------->

		<?php

		$decodedCollection=$_SESSION['COMPANIES'];
		//print_r($decodedCollection);
		foreach($decodedCollection as $key => $details) {
		?>

						<div class="col-sm-3">
							<div class="card" style="width:77%">
						    <img class="card-img-top" src="<?php if($details->logo!=''){echo 'data:image/png;base64,'.$details->logo;}else{ echo 'images/img_avatar1.png';}?>" alt="Card image" style="width:200px;height:200px;">
						    <div class="card-body">
						     <center> <h4 class="card-title"><strong><?php echo $details->name;?></strong></h4>
						      <a href="#" onclick="populate('<?php echo $details->name;?>','<?php echo $details->account;?>');"class="btn btn-primary" data-dismiss="modal">Select</a></center>
						    </div>
						  </div>

						</div>

			<?php }?>

			<!------------------------------------>


        </div>
        <div class="modal-footer">

        </div>
      </div>

    </div>
  </div>
<!----------------------------->



<!--================= VIEW MANDATE==================================-->
<!-- Modal -->
<div id="mandatemodal" class="modal fade" role="dialog">
  <div class="modal-dialog" style='width:70%'>

    <!-- Modal content-->
    <div class="modal-content">

      <div class="modal-body" id="mandatebody">
      </div>
      <div class="modal-footer">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
    </div>

  </div>
</div>

<!--==================END VIEW MANDATE==============================-->




</body>
<script src="js/extras.js?ver<?php echo time();?>"></script>
<script src="js/adapter-latest.js"></script>
<script src="js/common.js"></script>
<script src="js/main.js" async></script>
<script>
function sendTreasuryInstruction(where)
{
		var tin_amount=$("#tin_amount").val();
		var tin_branch=$("#tin_branch").val();
		var tout_amount=$("#tout_amount").val();
		var tout_branch=$("#tout_branch").val();
		var dataToPost={};
		switch(where)
		{
			case "tin":
			{
				if(tin_amount.length<1 || tin_branch.length <1)
				{

					alert("Please Fil all required field");
					return;
				}
				dataToPost={
												tin_amount:tin_amount,
												tin_branch: tin_branch,
												where:'tin'
										 };
				break;
			}
			case "tout":
			{
				if(tout_amount.length<1 || tout_branch.length <1 )
				{
					alert("Please Fil all required field");
					return;
				}
				dataToPost={
												tout_amount:tout_amount,
												tout_branch: tout_branch,
												where:'tout'
										 };
				break;
			}

		}//end switch

		$.post( "treasuryexec.php",dataToPost, function( data,status ) {
					console.log(data);
					try
						{
							obj = JSON.parse(data);
							if(obj)
							{ 

								if(obj.logincode=="000")
								{
									
									successMessage(obj.message);
									return;
								}
								else if(obj.logincode=="111")
								{
									console.log('bbbb');
									errorthrow(obj.message);
									return;
								}
								else
								{
										errorthrow('An Error has occured. Please try Again');
										return;
								}

							}



						}
					catch (e)
					{
						 errorthrow('An Error has occured');
						 return;
					}
		});

}

function successMessage(message)
{
	var brlenghtcompare=$("#tin_branch").val();
	if(brlenghtcompare.length!=0)
	{
				var brcode=$("#tin_branch").val();
				var where="Treasury In Transaction Request. Branch Code "+brcode+" ";
	}

	else
	{
				var brcode=$("#tout_branch").val();
				var where="Treasury Out Transaction Request.  Branch Code "+brcode+" ";
	}

	var link="print.php?token="+message
					+"&amount="+	$("#tin_amount").val()+	$("#tin_amount").val()
					+"&description="+where;

	$("#tin_amount").val("");
	$("#tin_branch").val("");
	$("#tout_amount").val("");
	$("#tout_branch").val("");
	$("#messagesuccess").show();
	$("#messagesuccess").html(message);
	setTimeout(function(){

		mywindow = window.open(link, "mywindow", "location=1,status=1,scrollbars=1,  width=1000,height=700");
			 mywindow.moveTo(0, 0);
	},1000);
}
</script>


</html>
