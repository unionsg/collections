<?php
include "classes/sendrequest.php";
include "config.php";
session_start();
if($_SERVER['REQUEST_METHOD']=="POST")
{
      //send api request
       $data=$_POST['data'];
       $method=$_POST['method'];
       $request= new sendRequest($data,$method,$ip.'user/login');
	   
       $response=$request->send();
       //echo $response;
       $decodedResponse=json_decode($response);
       $loginCode=$decodedResponse->loginCode;
       $message=$decodedResponse->message;
       if($loginCode=="000")
       {
         //login successfull
       $_SESSION['USERNAME'] =   $decodedResponse->username;
       $_SESSION['BRANCH'] = $decodedResponse->branch;
       $_SESSION['SESSIONID'] = $decodedResponse->sessionId;
       $_SESSION['HOSTNAME'] = $decodedResponse->hostname;
       $_SESSION['MACHINEIP'] = $decodedResponse->machineIp;
       $_SESSION['TRANSENQ'] = $decodedResponse->transEnq;
       $_SESSION['DEPARTMENT'] = $decodedResponse->department;
       $_SESSION['ACC'] = $decodedResponse->ac;
       $_SESSION['SUBBRANCH'] = $decodedResponse->subBranch;
       $_SESSION['EMPID'] = $decodedResponse->employeeId;
       $_SESSION['POSTINGDATE'] = $decodedResponse->postDate;
       $_SESSION['BRANCHDESC'] = $decodedResponse->brDesc;
       $_SESSION['FULLNAME'] = $decodedResponse->fullName;

       //getting collection
       $request= new sendRequest(null,'GET',$ip.'collections');
       $response=$request->send();
       //get response
       $_SESSION['COMPANIES']=json_decode($response)->data;

       //getting branches
       $request= new sendRequest(null,'GET',$ip.'/info/lov-codes?codeTypeEnum=BRA');
       $response=$request->send();
       $_SESSION['BRANCHES']=json_decode($response)->bankCodes;


       $arr = array('logincode' => $loginCode,
                    'message' => $message);
       echo json_encode($arr);
       exit();
     }
     else
     {
       //login fail
        $arr = array('logincode' => $loginCode,
                     'message' => $message);
        echo json_encode($arr);
        exit();

     }
}

?>
