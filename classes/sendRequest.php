<?php

class sendRequest
{
    var $data;
    var $method;
    var $url;
    var $port;
    
    function __construct($data, $method,$url)
    {
         $this->data = $data;
         $this->method = $method;
         $this->url = $url;
    }

    
	function send()
	{
		
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
		//CURLOPT_PORT => "8680",
		CURLOPT_URL => $this->url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => $this->method,
		CURLOPT_POSTFIELDS => $this->data,
		CURLOPT_HTTPHEADER => array(
		"Cache-Control: no-cache",
		"Content-Type: application/x-www-form-urlencoded"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		if ($err) {
		return "cURL Error #:" . $err;
		} else {
		return $response;
		}
		
		
	}



}//end of class.
?>
